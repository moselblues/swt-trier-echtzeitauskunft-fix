#!/usr/bin/python
from datetime import datetime
from json import loads
import httplib
import sys
import math

TESTDATA = '''[0,1,"Up8Lb0g",3,9,10,"Up8Lb0g",3,2,0,1,"Up8J1SA",3,5,4,"Up8J1SA",3,2,0,1,"Up8EVuA",3,5,7,"Up8EVuA",3,2,0,1,"Up8Oo5g",3,5,6,"Up8Oo5g",3,2,0,1,"Up8GZjg",3,5,4,"Up8GaCw",3,2,0,1,"Up8DMeg",3,9,8,"Up8DiNI",3,2,0,1,"Up8LNLA",3,5,7,"Up8LNLA",3,2,0,1,"Up8C91A",3,5,4,"Up8Da4Y",3,2,0,1,"Up8Hxcg",3,5,6,"Up8Hxcg",3,2,0,1,"Up8NRAg",3,5,4,"Up8NRAg",3,2,10,1,["java.util.ArrayList/4159755760","com.initka.onlineinfo.server.datamodel.DepartureInformation/276724449","java.util.Date/3385151746","Lud.-Erhard-Ring,Tarforst","83","Feyen, Weismark","Feyen, St. Medard","Wilhelm-Leuschner-Str.","85","Pluwig"],0,7]'''

class SWTRemoteException(Exception):
    pass

class BusTime:
    DELAYED_THRESHOLD = 30

    @staticmethod
    def from_gwt_long(inp):
        base = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$_"
        return sum(base.index(inp[p]) * (len(base) ** (len(inp) - p - 1)) for p in range(len(inp)))

    @classmethod
    def decode_time(self, t):
        return datetime.fromtimestamp(self.from_gwt_long(t)/1000)
        
    def __init__(self, data, _1, time1, _2, line, destination, time2, _3, _4,  offset = 0):
        self.time_planned = self.decode_time(time1)
        self.time_actual = self.decode_time(time2)
        self.line = data[line-1]
        self.destination = unicode(data[destination-1].encode('ascii', 'ignore'), 'iso-8859-1')
        self.offset = offset
            
    def get_actual_minutes_from_now(self):
        diff = self.time_actual - datetime.now()
        return round(diff.days * 24 * 60 + diff.seconds / 60.0 - self.offset / 60.0)

    def get_planned_minutes_from_now(self):
        diff = self.time_planned - datetime.now()
        return round(diff.days * 24 * 60 + diff.seconds / 60.0 - self.offset / 60.0)

    def get_status(self):
        diff = self.time_actual - self.time_planned
        delay = diff.days * 24 * 60 * 60 + (self.time_actual - self.time_planned).seconds
        if delay < 0:
            return -1
        if delay > self.DELAYED_THRESHOLD:
            return 1
        return 0
        
    def __str__(self):
        tp = self.time_planned.strftime("%H:%M")
        ta = self.time_actual.strftime("%H:%M")
        if ta == tp:
            return "Linie %s mit Ziel %40s um %s Uhr (noch %d)" % (self.line, self.destination, ta, self.get_actual_minutes_from_now())
        else:
            return "Linie %s mit Ziel %40s um %s ist %s Uhr (noch %d)" % (self.line, self.destination, tp, ta, self.get_actual_minutes_from_now())

class SWTBusList:
    NUM_ARGS = 9

    @staticmethod
    def get_data(stopcode):
        conn = httplib.HTTPConnection('212.18.193.124')
        headers = {
            'X-GWT-Module-Base': 'http://212.18.193.124/onlineinfo/onlineinfo/',
            'X-GWT-Permutation': 'D8AB656D349DD625FC1E4BA18B0A253C',
            'Content-Type': 'text/x-gwt-rpc; charset=UTF-8'}
        body = '5|0|6|http://212.18.193.124/onlineinfo/onlineinfo/|7E201FB9D23B0EA0BDBDC82C554E92FE|com.initka.onlineinfo.client.services.StopDataService|getDepartureInformationForStop|java.lang.String/2004016611|%s|1|2|3|4|1|5|6|' % stopcode
        conn.request("POST", "/onlineinfo/onlineinfo/stopData", body, headers)
        r1 = conn.getresponse()
        inp = r1.read()
        if not inp.startswith("//OK"):
            raise SWTRemoteException("Received Response from SWT Web Service indicating an error.")
        data = inp[4:].replace("'", '"')
        return data

    @classmethod
    def parse_data(self, data, offset = 0):
        everything = loads(data)
        inner = everything[-3]

        if len(everything) < self.NUM_ARGS:
            return []

        buslist = []
        x = 0
        while True:
            if everything[self.NUM_ARGS*x] != 0:
                break
            c = everything[self.NUM_ARGS*x+1:self.NUM_ARGS*x+self.NUM_ARGS]
            buslist.append(BusTime(inner, *c, offset = offset))
            x += 1
        return buslist
        
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Please provide stop code as first argument, e.g. UNI, HBF etc.; Write --test to use testing data."
        sys.exit(1)

    stop = sys.argv[1]
    if stop == '--test':
        data = TESTDATA
    else:
        data = SWTBusList.get_data(stop)
        
    bl = SWTBusList.parse_data(data)
        
    for b in sorted(bl, key=lambda x: x.get_actual_minutes_from_now()):
        print b

